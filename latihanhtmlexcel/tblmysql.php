<!DOCTYPE html>
<html>
<head>
    <title>TEST EXPORT TABLE</title>
    <link href="dist/css/tableexport.min.css" rel="stylesheet">
    <link href="./examples.css" rel="stylesheet">
</head>
<body>

 <?php
 //begin creating table
require_once("mysql_connect2.php");

$tblname = "2311p";                 #table name
$sqlq = "SELECT * FROM ".$tblname;  #database query
$sqlread= $rundb->Query($sqlq);
?>

<table id="test-table" border='1'>
<?php 
while ($rows = $rundb->FetchAssoc($sqlread)){   
    if (!isset($key)) {
        echo "<tr colspan='2'>"; 
        foreach ($rows as $key => $value) {
             echo "<th>".$key."</th>";
        }
        echo "</tr>";
    }
    unset($value);
    echo "<tr>";
    foreach ($rows as $value) {
        echo "<td>".$value."</td>";
    }
    echo "</tr>";        
unset($value);
}
//end create table
/**  --THIS IS OLD VERSION--
while ($rows = $rundb->FetchAssoc($sqlread)){   
    if ($i==0) {
        $i++;
        echo "<tr colspan='2'>"; 
        foreach ($rows as $key => $value) {
             echo "<th>".$key."</th>";
        }
        echo "</tr>";
    }
    unset($value);
    unset($key);
    echo "<tr>";
    foreach ($rows as $key => $value) {
        echo "<td>".$value."</td>";
    }
    echo "</tr>";    
}
unset($value);
//end create table
--THIS IS OLD VERSION--  **/
?>
</table> 
<script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="bower_components/js-xlsx/dist/xlsx.core.min.js"></script>
<script type="text/javascript" src="bower_components/blobjs/Blob.min.js"></script>
<script type="text/javascript" src="bower_components/file-saverjs/FileSaver.min.js"></script>
<script type="text/javascript" src="dist/js/tableexport.min.js"></script>
<script type="text/javascript" src="assets/js/analytics.js"></script>

<script>

    // Default sheetname if `sheetname: false|null|undefined|""`
    TableExport.prototype.defaultSheetname = 'fallback-name';
    // **** jQuery **************************
    // $.fn.tableExport.defaultSheetname = 'fallback-name';
    // **************************************

    var testTable = document.getElementById('test-table');
    new TableExport(testTable, {
        sheetname: 'worksheet',
        position: 'top' 
    });
    

</script>


</body>
</html>